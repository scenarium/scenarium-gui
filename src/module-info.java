import io.scenarium.gui.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.gui {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.core;

	exports io.scenarium.gui;

}
