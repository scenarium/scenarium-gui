/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.gui;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.function.Supplier;

import io.scenarium.pluginManager.ModuleManager;

import org.beanmanager.editors.container.ArrayEditor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import io.scenarium.core.Scenarium;

import com.googlecode.junittoolbox.ParallelParameterized;

@RunWith(value = ParallelParameterized.class)
public class StreamAndTextTest {
	private static final int NB_RUN = 10;
	private static final int SEED = 369;
	private static final int MAX_LEVEL = 3;
	private static final int MAX_SIZE = 10;
	private static final double NULL_RATIO = 0.1;
	private static final Random RAND = new Random(SEED);
	private static final Class<?>[] TESTED_TYPES = new Class<?>[] { /* CompressedImage.class */ };

	public StreamAndTextTest() {
		ModuleManager.birth(Scenarium.workspaceDirectory());
	}

	@Parameter
	public Class<?> type;

	// Single parameter, use Object[]
	@Parameters(name = "{index}: type - {0}")
	public static Class<?>[] data() {
		return TESTED_TYPES;
	}

	private static String randomString(Random rand, int count) {
		String s = new String();
		int nbChar = rand.nextInt(count);
		for (int i = 0; i < nbChar; i++) {
			char c = (char) rand.nextInt();
			while (Character.getType(c) == Character.SURROGATE || c == '\n' || c == '\r')
				c = (char) rand.nextInt();
			s = s + c;
		}
		return s;
	}

	private static byte[] randomBytes(Random rand, int count) {
		byte[] out = new byte[rand.nextInt(count)];
		rand.nextBytes(out);
		return out;
	}

	@Test
	public void testType() {
		assertTrue(checkPropertyArray(RAND, MAX_LEVEL, MAX_SIZE, NULL_RATIO, this.type, () -> generateRandomValue(this.type, RAND, MAX_SIZE, TESTED_TYPES)));
	}

	public static Object generateRandomValue(Class<?> type, Random rand, int maxSize, Class<?>[] testedTypes) {
		// if (type.equals(CompressedImage.class)) {
		// return new CompressedImage(randomString(rand, 20), randomBytes(rand, 200));
		// }
		throw new IllegalArgumentException("No generator for type: " + type);
	}

	private static boolean checkPropertyArray(Random rand, int maxlevel, int maxSize, double nullRatio, Class<?> arrayType, Supplier<?> supplier) {
		try {
			int textError = 0;
			for (int k = 0; k < NB_RUN; k++) {
				Class<?> type = arrayType;
				for (int i = 0; i < maxlevel; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				// Text test
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass(), "");
				ed.setValueFromObj(array);
				String text = ed.getAsText();
				ed.setAsText(text);
				Object resValue = ed.getValue();
				if (!compareArray(array, resValue)) {
					textError++;
					ed.setAsText(text);
					ed.setValueFromObj(array);
					text = ed.getAsText();
					ed.setAsText(text);
					Object v = ed.getValue();
					System.out.println(v);
					System.out.println(compareArray(array, resValue));
				}
			}
			int streamError = 0;
			for (int k = 0; k < NB_RUN; k++) {
				int level = 1 + (int) (rand.nextDouble() * maxlevel);
				Class<?> type = arrayType;
				for (int i = 0; i < level - 1; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass(), "");
				// Flow test
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try (DataOutputStream dos = new DataOutputStream(baos)) {
					ed.writeValueFromObj(dos, array);
				}
				try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()))) {
					Object array2 = ed.readValue(dis);
					if (!compareArray(array, array2))
						streamError++;
				}
			}
			if (textError != 0 || streamError != 0) {
				if (textError == 0)
					System.err.println(arrayType.getSimpleName() + ": Text ok, Stream fail");
				else if (streamError == 0)
					System.err.println(arrayType.getSimpleName() + ": Text fail, Stream ok");
				else
					System.err.println(arrayType.getSimpleName() + ": both failed");
				return false;
			}
			return true;
		} catch (Exception e) {
			System.err.println(arrayType.getSimpleName() + ": Exception");
			e.printStackTrace();
			return false;
		}
	}

	private static boolean compareArray(Object a1, Object a2) {
		if (a1 == null ^ a2 == null)
			return false;
		if (a1 == null && a2 == null)
			return true;
		if (a1.getClass().isArray() ^ a2.getClass().isArray())
			return false;
		if (a1.getClass().isArray()) {
			if (Array.getLength(a1) != Array.getLength(a2))
				return false;
			for (int i = 0; i < Array.getLength(a1); i++)
				if (!compareArray(Array.get(a1, i), Array.get(a2, i)))
					return false;
		} else
			return a1.equals(a2);
		return true;
	}

	private static void populateArray(Object array, Random rand, Supplier<?> supplier, double nullRatio) {
		int size = Array.getLength(array);
		Class<?> subType = array.getClass().getComponentType();
		for (int i = 0; i < size; i++)
			if (!subType.isArray())
				Array.set(array, i, !subType.isPrimitive() && rand.nextDouble() < nullRatio ? null : supplier.get());
			else if (rand.nextDouble() < nullRatio)
				Array.set(array, i, null);
			else {
				Class<?> subsubType = subType.getComponentType();
				Object subArray = Array.newInstance(subsubType, (int) (rand.nextDouble() * 10));
				populateArray(subArray, rand, supplier, nullRatio);
				Array.set(array, i, subArray);
			}
	}
}
